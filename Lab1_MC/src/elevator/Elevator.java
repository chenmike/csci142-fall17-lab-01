package elevator;

/**
 * Elevator class to simulate the operation of an elevator.  Details
 * associated with opening and closing of the elevator doors, entry and
 * exit of people to and from the elevator, the number of people in, 
 * entering or leaving the elevator, and the timing of the movement of
 * the elevator are all "abstracted" out of the problem, encompassing 
 * all of these actions into a single "move()" operation.
 * 
 * @author dplante
 *
 */
public class Elevator implements ElevatorOperations
{
	/**
	 * Initializing default variables
	 */
	private int NO_VAL=-1;
	private int[]myUpButton; //up button array
	private int[]myDownButton; //down button array
	private boolean[]myInButton; // in button array
	private int mySeqNumber; //sequence of up and down buttons pushed
	private int myOrderSeq;  //the next up or down buttons in sequence
	private int mycheckFloor;// the next floor the elevator should move to
	private int myCurrentFloor; //current floor value for getDirection() return
	private int myDirection; // direction the elevator is traveling at
	private int myNumberOfFloors;// amount of total floors
	
	/**
	 * Default constructor setting the number of floors for
	 * the elevator to five (5)
	 */
	public Elevator()
	{
		this(5);
	}
	
	/**
	 * Constructor setting the number of floors
	 * 
	 * @param numFloors total number of floors
	 */
	public Elevator(int numFloors)
	{
		myNumberOfFloors = numFloors; 
		myUpButton= new int[]{-1,-1,-1,-1,-1}; 
		myDownButton= new int[] {-1,-1,-1,-1,-1};
		myInButton= new boolean[myNumberOfFloors];
		mySeqNumber=0;
		myOrderSeq=1;
		mycheckFloor=NO_VAL;
		myCurrentFloor=NO_VAL;
		myDirection=NOT_SET;
	}
	/**
	 * checking if the pushed up button is a valid choice or not
	 * 
	 * @param the floor being pressed
	 * @return the boolean of the input floor
	 */
	public boolean pushUp(int floor)
	{
		boolean checkup = true;
		int f=floor -1;
		if(floor<myNumberOfFloors&&floor>0)
		{
			for(int i=0; i<myUpButton.length;i++)
			{
				if(myUpButton[f]!=-1)
				{
					checkup=false;
				}
			}
		}else{checkup=false;}
		if(checkup==true)
		{
			mySeqNumber++;
			myUpButton[f]=mySeqNumber;
			//myDirection=UP;
		}
		return checkup;
	}
	/**
	 * checking if the pushed down button is a valid choice or not
	 * 
	 * @param the floor being pressed
	 * @return the boolean of the input floor
	 */
	public boolean pushDown(int floor)
	{
		boolean checkdown = true;
		int f=floor -1;
		if(floor<=myNumberOfFloors&&floor>1)
		{
			for(int i=0; i<myDownButton.length;i++)
			{
				if(myDownButton[f]!=-1)
				{
					checkdown=false;
				}
			}
		}else{checkdown=false;}
		if(checkdown==true)
		{
			mySeqNumber++;
			myDownButton[f]=mySeqNumber;
			//myDirection=DOWN;
		}
		return checkdown;
	}
	/**
	 * checking if the pushed in button is a valid choice or not
	 * 
	 * @param the floor being pressed
	 * @return the boolean of the input floor
	 */
	public boolean pushIn(int floor)
	{
		boolean checkin = true;
		int f=floor -1;
		if(floor>0&&floor<=myNumberOfFloors)
		{
			for(int i=0; i<myInButton.length;i++)
			{
				if(myInButton[f]==true)
				{
					checkin=false;
				}
				if(myDirection==UP && floor<=myCurrentFloor)
				{
					checkin=false;
				}
				if(myDirection==DOWN && floor>=myCurrentFloor)
				{
					checkin=false;
				}	
			}
		}else{checkin=false;}
		if (checkin==true)
		{
			myInButton[f]=true;
		}
		return checkin;
	}
	/**
	 * Determines how to check the in/up/down arrays and sequences to determine 
	 * which floor the elevator moves to based on the direction
	 * 
	 * @return the floor the elevator moves to
	 */
	public int move()
	{
		
		if(myDirection==NOT_SET)
		{
			mycheckFloor=checkNotSet();
		}
		else if(myDirection==DOWN)
		{
			mycheckFloor=checkDown();
		}
		else if(myDirection==UP)
		{
			mycheckFloor=checkUp();
		}
		myCurrentFloor=mycheckFloor;
		return mycheckFloor;
	}
	/**
	 * How to check the in/up/down arrays and sequences to determine 
	 * which floor the elevator moves to if direction is up
	 * 
	 * @return the next floor the elevator moves to
	 */
	public int checkUp()
	{
		for(int i =myCurrentFloor-1; i<=myNumberOfFloors-1;i++)
		{
			if(myInButton[i]==true)
			{
				myInButton[i]=false;
				myUpButton[i]=NO_VAL;
				myDirection=NOT_SET;
				myDownButton[i]=NO_VAL;
				return i+1;
			}
			else if(myUpButton[i]>=myOrderSeq)
			{
				if(myUpButton[i]==myOrderSeq)
				{
					myOrderSeq++;
				}
				myUpButton[i]=NO_VAL;
				return i+1;
			}
		}
		for(int g = myNumberOfFloors-1;g>0;g--)
		{
			if(myDownButton[g]>=myOrderSeq)
			{
				myOrderSeq++;
				myDirection=DOWN;
				myDownButton[g]=NO_VAL;
				return g+1;
			}
		}
		return mycheckFloor;
	}
	/**
	 * How to check the in/up/down arrays and sequences to determine 
	 * which floor the elevator moves to if direction is down
	 * 
	 * @return the next floor the elevator moves to
	 */
	public int checkDown()
	{
		for(int i=myCurrentFloor-1;i>=0;i--)
		{
			//check in buttons
			 if(myInButton[i]==true)
			 {
				 myInButton[i]=false;
				 myDownButton[i]=NO_VAL;
				 myDirection=NOT_SET;
				 myUpButton[i]=NO_VAL;
				 return i+1;
			 }
			 //check down buttons
			 else if(myDownButton[i]>=myOrderSeq)
			 {
				 if(myDownButton[i]==myOrderSeq)
				 {
					 myOrderSeq++;
				 }
				 myDownButton[i]=NO_VAL;
				 return i+1;
			 }
		}
		// check up buttons
		for(int g=0;g<myNumberOfFloors-1;g++)
		{
			if(myUpButton[g]>=myOrderSeq)
			{
				myDirection=UP;
				myUpButton[g]=NO_VAL;
				myOrderSeq++;
				return g+1;
			}
		}
		return mycheckFloor;
	}
	/**
	 * How to check the in/up/down arrays and sequences to determine 
	 * which floor the elevator moves to if direction is not set
	 * 
	 * @return the next floor the elevator moves to
	 */
	public int checkNotSet()
	{
		for(int i =0; i<=myNumberOfFloors-1;i++)
		{
			// check when up and down buttons pressed on same floor
			if(myUpButton[i]!=-1&&myDownButton[i]!=-1)
			{
			//when up button comes first
			if(myUpButton[i]<myDownButton[i])
			{
				if(myUpButton[i]==myOrderSeq)
				{
					myOrderSeq++;
				}
				myDirection=UP;
				myUpButton[i]=NO_VAL;
				return i+1;
			}
			//when down button comes first
			if(myUpButton[i]>myDownButton[i])
			{
				if(myUpButton[i]==myOrderSeq)
				{
					myOrderSeq++;
				}
				myDirection=DOWN;
				myDownButton[i]=NO_VAL;
				return i+1;
			}
		}
		//check up buttons
		if(myUpButton[i]>=myOrderSeq)
		{
			if(myUpButton[i]==myOrderSeq)
			{
				myOrderSeq++;
			}
			myDirection= UP;
			myUpButton[i]=NO_VAL;
			return i+1;
		}
		//check down buttons
		else if(myDownButton[i]>=myOrderSeq)
		{
			if(myDownButton[i]==myOrderSeq)
			{
				myOrderSeq++;
			}
			myDirection=DOWN;
			myDownButton[i]=NO_VAL;
			return i+1;
		}
		}
		// check in buttons
		for( int g= myNumberOfFloors-1;g>=0;g--)
		{
			if(myInButton[g]==true)
			{
				myInButton[g]=false;
				myDirection=NOT_SET;
				myDownButton[g]=NO_VAL;
				myUpButton[g]=NO_VAL;
				return g+1;
			}
		}
		return mycheckFloor;
		
	}
	/**
	 * Gets direction of elevator
	 * 
	 * @return direction elevator moves to
	 */
	public int getDirection()
	{
		return myDirection;
	}
	public int getFloor() 
	{
		return 0;
	}

}
