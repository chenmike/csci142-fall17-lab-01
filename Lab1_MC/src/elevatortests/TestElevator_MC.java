package elevatortests;

import elevator.Elevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Some sample Elevator class test cases!
 * 
 * @author mchen
 *
 */

public class TestElevator_MC 
{
	private Elevator myElevator;
	
	/**
	 * An elevator with 5 floors will be set up new before running 
	 * each test
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		myElevator = new Elevator(5);
	}

	
	/**
	 * Test when up/down buttons are pushed in a way that makes elevator goes down and then up
	 * 
	 */
	@Test
	public void testDownUpChange() 
	{
		assertTrue(myElevator.pushDown(2));
		assertEquals("Move should place elevator on second floor", myElevator.move(), 2);
		assertTrue(myElevator.pushIn(1));
		assertTrue(myElevator.pushDown(3));
		assertTrue(myElevator.pushUp(4));
		assertTrue(myElevator.pushUp(3));
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);	
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertTrue(myElevator.pushIn(1));
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertTrue(myElevator.pushIn(5));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertFalse(myElevator.pushIn(5));
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
		
	}
	/**
	 * Test when buttons are pushed in a row that makes elevator goes up and then down
	 * 
	 */
	@Test
	public void testUpDownChange() 
	{
		assertTrue(myElevator.pushUp(1));
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertTrue(myElevator.pushDown(3));
		assertTrue(myElevator.pushUp(2));
		assertTrue(myElevator.pushDown(2));
		assertTrue(myElevator.pushUp(4));
		assertTrue(myElevator.pushIn(3));
		assertEquals("Move should place elevator on second floor", myElevator.move(), 2);
		assertTrue(myElevator.pushIn(5));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertTrue(myElevator.pushIn(1));
		assertEquals("Move should place elevator on second floor", myElevator.move(), 2);
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
	}
	@Test
	public void testMultiplePassengers() 
	{
		assertTrue(myElevator.pushUp(3));
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertTrue(myElevator.pushIn(5));
		assertTrue(myElevator.pushIn(4));
		assertFalse(myElevator.pushIn(1));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
		assertTrue(myElevator.pushUp(1));
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertTrue(myElevator.pushIn(5));
		assertTrue(myElevator.pushIn(4));
		assertTrue(myElevator.pushIn(3));
		assertTrue(myElevator.pushIn(2));
		assertEquals("Move should place elevator on second floor", myElevator.move(), 2);
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
	}
	/**
	 * Test directions
	 */
	@Test
	public void testDirections() 
	{
		assertEquals("Direction should not be set yet", myElevator.getDirection(), Elevator.NOT_SET);
		assertTrue(myElevator.pushDown(4));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertTrue(myElevator.pushIn(1));
		assertEquals("Direction should be set to DOWN", myElevator.getDirection(), Elevator.DOWN);
		assertTrue(myElevator.pushUp(3));
		assertEquals("Direction should be set to DOWN", myElevator.getDirection(), Elevator.DOWN);
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertEquals("Direction should be set to UP", myElevator.getDirection(), Elevator.UP);
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertEquals("Direction should not be set yet", myElevator.getDirection(), Elevator.NOT_SET);
		assertEquals("Move should keep elevator on third floor", myElevator.move(), 3);
	}
	/**
	 * Test what happens when all up buttons are pushed in different sequence
	 */
	@Test
	public void testAllGoingUp() 
	{
		assertTrue(myElevator.pushUp(1));
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertTrue(myElevator.pushUp(4));
		assertTrue(myElevator.pushIn(5));
		assertTrue(myElevator.pushUp(1));
		assertTrue(myElevator.pushUp(2));
		assertTrue(myElevator.pushUp(3));
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertTrue(myElevator.pushIn(5));
		assertEquals("Move should place elevator on second floor", myElevator.move(), 2);
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
	}
	/**
	 * Test what happens when all down buttons are pushed in different sequence
	 */
	@Test
	public void testAllGoingDown() 
	{
		assertTrue(myElevator.pushDown(5));
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
		assertTrue(myElevator.pushIn(1));
		assertTrue(myElevator.pushDown(3));
		assertTrue(myElevator.pushDown(4));
		assertTrue(myElevator.pushDown(2));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertEquals("Move should place elevator on second floor", myElevator.move(), 2);
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
	}
	/**
	 * Test cases in which the sequence of pushed buttons decide direction of elevator
	 */
	@Test
	public void testSequencePriority() 
	{
		assertTrue(myElevator.pushDown(4));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertTrue(myElevator.pushDown(5));
		assertTrue(myElevator.pushIn(1));
		assertEquals("Direction should be set to DOWN", myElevator.getDirection(), Elevator.DOWN);
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertTrue(myElevator.pushDown(4));
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
		assertTrue(myElevator.pushIn(1));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
	}
	/**
	 * Test buttons that should not move elevator
	 */
	@Test
	public void testNotMoving() 
	{
		assertTrue(myElevator.pushDown(4));
		assertEquals("Move should place elevator on fourth floor", myElevator.move(), 4);
		assertFalse(myElevator.pushIn(4));
		assertTrue(myElevator.pushIn(1));
		assertFalse(myElevator.pushIn(1));
		assertFalse(myElevator.pushIn(5));
		assertEquals("Move should place elevator on first floor", myElevator.move(), 1);
		assertTrue(myElevator.pushUp(3));
		assertFalse(myElevator.pushUp(3));
		assertEquals("Move should place elevator on third floor", myElevator.move(), 3);
		assertFalse(myElevator.pushIn(1));
		assertTrue(myElevator.pushIn(5));
		assertEquals("Move should place elevator on fifth floor", myElevator.move(), 5);
	}
}
